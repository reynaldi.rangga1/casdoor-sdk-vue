export type CommentRaw = {
  name: string;
  email: string;
  message: string;
};
