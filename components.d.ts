// generated by unplugin-vue-components
// We suggest you to commit this file into source control
// Read more: https://github.com/vuejs/vue-next/pull/3399

declare module 'vue' {
  export interface GlobalComponents {
    AppSidebar: typeof import('./src/components/AppSidebar.vue')['default']
    PageHeader: typeof import('./src/components/PageHeader.vue')['default']
    TableActions: typeof import('./src/components/TableActions.vue')['default']
    ToastMessage: typeof import('./src/components/ToastMessage.vue')['default']
    VIcon: typeof import('./src/components/VIcon/VIcon.vue')['default']
  }
}

export { }
