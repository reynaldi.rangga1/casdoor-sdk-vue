import {defineStore} from 'pinia';

export * from './layout';

export const useStore = defineStore('main', {});

export default useStore;
